<?php 

	/* Отключаем админ панель для всех пользователей. */
	  show_admin_bar(false);
	
	/* Миниатюры. */
	add_theme_support( 'post-thumbnails' ); 
	  
	// подключение бутстрап меню класса
	require_once('wp_bootstrap_navwalker.php');
	// подключение пагинации
	include("pagination.php");
	// подключение пагинации
	include("the_excerpt.php");

	// новое меню
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'THEMENAME' ),
	) );
	
	//show links editor
	add_filter( 'pre_option_link_manager_enabled', '__return_true' );

	//вставка в контент пхп [exec]code[/exec]
	function exec_php($matches){
		eval('ob_start();'.$matches[1].'$inline_execute_output = ob_get_contents();ob_end_clean();');
		return $inline_execute_output;
	}
	
	function inline_php($content){
		$content = preg_replace_callback('/\[exec\]((.|\n)*?)\[\/exec\]/', 'exec_php', $content);
		$content = preg_replace('/\[exec off\]((.|\n)*?)\[\/exec\]/', '$1', $content);
		return $content;
	}
	add_filter('the_content', 'inline_php', 0);
	
	//убрать лишние переносы <BR>
	remove_filter( 'the_content', 'wpautop' );
	remove_filter( 'the_excerpt', 'wpautop' );


	

	// ====== Сайдбары ====== 
	function register_wp_sidebars() { 
		/* надпись в хедере */
		register_sidebar(
			
				array(
					'id' => 'blog_links',
					'name' => 'blog_links', 
					'description' => 'BlogPress Releases', 
					'before_widget' => '<div id="%1$s" class="side widget %2$s">', 
					'after_widget' => '</div>',
					'before_title' => '<h3 class="widget-title">', 
					'after_title' => '</h3>'
				)
		);

	 
	}
	 
	add_action( 'widgets_init', 'register_wp_sidebars' );
	
	
	
	// === Произвольные поля === \\
	function custom_field() {
		add_meta_box( 'custom_info', 'Post parameters', 'add_movieurl_func', 'post', 'normal', 'high'  );
	}
	add_action('add_meta_boxes', 'custom_field', 1);
	
	function add_movieurl_func( $post ){
		
		wp_nonce_field( basename( __FILE__ ), 'custompost_nonce' );
		
		?>
		<!--
		<p>
			<label style="font-weight: 600; display: block;" >
				Main page short story:
			</label>
			<textarea name="extra[short_main]" placeholder="Type short text, wich will be shown in main page" style="width:100%; color: #0073aa;"><?php echo get_post_meta($post->ID, 'short_main', 1); ?></textarea>
		</p>
		-->	
		<p>
			<label style="font-weight: 600; display: block;" >
				Geo:
			</label>
			<input type="text" name="extra[post_geo]" placeholder="Type here post geo location" value="<?php echo get_post_meta($post->ID, 'post_geo', 1); ?>" style="width:100%; color: #0073aa;">				
		</p>		
			
		<?php
	}

function save_customfield ( $postid ) {
		
		// проверяем, пришёл ли запрос со страницы с метабоксом
		if ( !isset( $_POST['custompost_nonce'] ) || !wp_verify_nonce( $_POST['custompost_nonce'], basename( __FILE__ ) ) )
			return $postid;
		// проверяем, является ли запрос автосохранением
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
			return $postid;
				
		update_post_meta($postid, 'post_geo', esc_attr($_POST['extra']['post_geo']));
		//update_post_meta($postid, 'short_main', $_POST['extra']['short_main']);			

			
		return $postid;
	}
	 
	add_action('save_post', 'save_customfield');

?>
