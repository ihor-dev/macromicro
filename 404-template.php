<?php get_header(); ?>
<div class="wrapper">
	<div class="section-header"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">

					<?php query_posts('page_id=274');
						if ( have_posts() ) : while ( have_posts() ) : the_post();?>							
							<?php the_content(); ?>
							<?php endwhile;  wp_reset_query(); ?>
					<?php endif; ?>
				</div>
			</div>				
		</div>
	</div>
</div>
<?php get_footer(); ?>