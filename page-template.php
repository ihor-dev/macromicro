<?php get_header(); ?>
<div class="wrapper">
	<div class="section-header"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
					
					<?php the_content(); ?>
				<?php endwhile; else: ?> 
					<p>
						<?php _e('Sorry, but page not found =('); ?>
					</p> 
				<?php endif; ?>
				</div>
			</div>				
		</div>
	</div>
</div>
<?php get_footer(); ?>