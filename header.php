<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="google-site-verification" content="vwMPNsJcSwCHWMaDW_QdmFRx9-wo7e3HfgZAemx2Nf0" />
		<link rel="shortcut icon" href="img/favicon.ico">
		<title>Macromicro | Visual Perspective for Workforce Analytics</title>

    <!-- Bootstrap core CSS -->
		<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
		<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/elements.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" />
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<!--[if IE]>
	<style>
		.showcase {
			 zoom: 0.6;
		}
	</style>
	<![endif]-->
	<?php wp_head(); ?> 
	</head>
	<body>

  <!-- Navbar
	============= -->



		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?=home_url();?>">Macromicro</a>
				</div>

				<?php // Collect the nav links, forms, and other content for toggling 
					wp_nav_menu( array(
						'menu'              => 'primary',
						'theme_location'    => 'primary',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse',
						'container_id'      => 'bs-example-navbar-collapse-1',
						'menu_class'        => 'nav navbar-nav',
						'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
						'walker'            => new wp_bootstrap_navwalker())
					);
				?>
				<ul class="nav navbar-nav navbar-right hidden-xs">
					<li class="content social"><a href="http://www.twitter.com/macromicrollc"><i class="fa fa-twitter fa-lg"></i></a>
					</li>
					<li class="content social"><a href="http://www.linkedin.com/company/macromicro"><i class="fa fa-linkedin fa-lg"></i></a>
					</li>
					<li id="search">
						<a href="<?=home_url();?>/contact-demo/">Contact + Demo</a>
					</li>
				</ul>

			</div>
		</div>

  