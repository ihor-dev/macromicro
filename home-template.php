
  <!-- Showcase	================ -->
		<div class="wrapper">
			<div id="wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<h1 class="animated slideInDown">Visual Perspective for Workforce Analytics</h1>
							<div class="list">
								<ul>
									<li class="animated slideInLeft first"><span class="li-bg"><i class="fa fa-check-circle"></i> <span>Proactive Organizational Planning</span></span>
									</li>
									<li class="animated slideInLeft second"><span class="li-bg"><i class="fa fa-check-circle"></i> <span>Evidence-Based Decision-Making</span></span>
									</li>
									<li class="animated slideInLeft third"><span class="li-bg"><i class="fa fa-check-circle"></i> <span>Workforce Data in Human Context</span></span>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-md-6 hidden-sm hidden-xs">
							<div class="showcase">
								<div class="iMac animated fadeInDown"></div>
								<div class="iPad animated fadeInLeft"></div>
			  <!--<div class="iPhone animated fadeInRight"></div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
	  <!-- Services	================ -->
				<div class="row">
					<div class="col-md-12">
						<div class="services">
							<ul>
								<li>
									<i class="fa fa-compass fa-2x"></i>
									<p>Navigate the Workforce<br /><a href="peoplemap.php">with PeopleMap...</a></p>
								</li>
								<li>
									<i class="fa fa-dashboard fa-2x"></i>
									<p>Retention Management<br /><a href="retentionmanagement.php">with OrgInsight...</a></p>
								</li>
								<li>
									<i class="fa fa-align-justify fa-2x"></i>
									<p>Spans & Layers<br /><a href="spansandlayers.php">with OrgInsight...</a></p>
								</li>
								<li>
									<i class="fa fa-star fa-2x"></i>
									<p>Spot Rising Leaders<br /><a href="risingleaders.php">with OrgInsight...</a></p>
								</li>
								<li>
									<i class="fa fa-cloud fa-2x"></i>
									<p>All in the Cloud<br /><a href="orginsight.php">OrgInsight + SAP HANA...</a></p>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="row">
	    <!-- Welcome message	================= -->
					<div class="col-md-8">
						<?php query_posts('page_id=145');
						if ( have_posts() ) : while ( have_posts() ) : the_post();?>							
							<?php the_content(); ?>
							<?php endwhile;  wp_reset_query(); ?>
						<?php endif; ?>


					</div>
		<!-- Last updated	================== -->
					<div class="col-md-4">
						<div class="block-header">
							<h2>
								News
							</h2>
						</div>
						
						<?php 
						$bookmarks = get_bookmarks(array(
							'orderby'  => 'id',
							'order'	=> 'DESC',
							'category_name' => 'Main',
							'limit' => 3
							));
						?>

						<div class="tab-content">
							<div class="tab-pane active" id="blog">
								<?	foreach($bookmarks as $bm):?>
								<div class="media">
									<a class="pull-left" href="<?=$bm->link_url;?>">
										<img class="media-object" src="<?=$bm->link_image; ?>">
									</a>
									<div class="media-body">
										<h4 class="media-heading"><a href="<?=$bm->link_url;?>"><?=$bm->link_name;?></a></h4>
										<?=$bm->link_description;?>
									</div>
								</div>
								<?endforeach?>								

								<a href="<?=home_url();?>/blog" class="read-more">More news...</a>
								
							</div>							
						</div>
					</div>
				</div>
				<HR>
				<div class="row">
					<div class="col-md-12">
						<p class="lead text-center">
							&#8220;OrgInsight provides effortless workforce analytics, which HR Executives, Finance and Line managers are able to utilize by themselves to better manage our workforce.&#8221; &emsp; — Juha Äkräs, Executive Vice President, Human Resources, Nokia
						</p>
						<HR>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/works3.jpg" class="img-responsive" alt="...">
							<div class="visit"></div>
							<div class="caption">
								<a href="newperspectives.php"><h4>1,000 or 500,000 People</h4></a>
								<p>This is the blueprint of your organization; the DNA of your workforce.</p>
								<p>Capture thousands of employees in a single window. Visually analyze the workforce and all its complexity in a single interface with precision.
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/works1.jpg" class="img-responsive" alt="...">
							<div class="visit"></div>
							<div class="caption">
								<a href="orginsight.php"><h4>See + Search Across Time</h4></a>
								<p>Search for attributes and individuals and see changes in these data over time.</p>
								<p>Visualization illuminates concentrations, patterns, trends, and insight you've never had in spreadsheets and static pie charts.
								</p>
							</div>
						</div>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/works2.jpg" class="img-responsive" alt="...">
							<div class="visit"></div>
							<div class="caption">
								<a href="orginsight.php"><h4>Compare Distributions</h4></a>
								<p>Compensation ratios in UK vs. France? Two seconds.</p>
								<p>Compensation ratios + UK/France + female/male + spans of control + this quarter/last quarter? Three seconds.
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/works4.jpg" class="img-responsive" alt="...">
							<div class="visit"></div>
							<div class="caption">
								<a href="newperspectives.php"><h4>Monitor Trends</h4></a>
								<p>Check-in quickly and easily on trends and patterns. Real-time, multivariate data is fed to our dashboard through the SAP HANA platform.
								</p>
								<p>Having your data in one place is revolutionary in terms of the insight it can provide.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
