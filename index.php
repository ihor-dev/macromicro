<?php 
	get_header();

	if(is_home()) { // HOME PAGE -------v

		include('home-template.php');		

	}elseif(is_page()) { // PAGES ------v

		include('page-template.php');

	}elseif (is_category('blog')) {

		include('blog-template.php');		

	}elseif (is_single()) {

		include('single-template.php');

	}elseif (is_404()) {

		include('404-template.php');
		
	}


	get_footer(); 
?>