	<!--Analytics--><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-33361407-1', 'macromicro.com');
  ga('send', 'pageview');

</script><!--Analytics-->
<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/507507.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->

<footer>
	<div class="container">
		<div class="row">
	    <!-- Contact Us =================  -->
			<div class="col-sm-4">
				<div class="headline"><h3>Where We're Located</h3></div>
				<div class="content">
					<p>Boston | Helsinki | Singapore</p><p>
						20 Rowes Wharf, Suite 409<br />
						Boston, Massachusetts 02110
					</p>				
				</div>
			</div>
		<!-- Social icons 	===================== -->
			<div class="col-sm-4">
				<div class="headline"><h3>Contact Us</h3></div>
				<div class="content social">
					<p><a href="mailto:info@macromicro.com">info@macromicro.com</a><BR>Tel +1 617 843 5030</p>
					<ul>
						<li><a href="http://www.twitter.com/macromicrollc"><i class="fa fa-twitter"></i></a>
						</li>
						<li><a href="http://www.linkedin.com/company/macromicro"><i class="fa fa-linkedin"></i></a>
						</li>
					</ul>
					
				</div>
			</div>	
		<!-- Subscribe 	=============== -->
			<div class="col-sm-4">
				<div class="headline"><h3>Schedule a Call</h3></div>
				<div class="content social">
					<a href="https://calendly.com/macromicro"><i class="fa fa-calendar fa-2x"></i></a>
				</div>
			</div>
		</div>
	</div>
	</div>
</footer>
  <!-- Legal 
  ============= -->
<div class="legal">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p>&copy; 2015 Macromicro LLC. All Rights Reserved. Patents Pending.<BR>SAP® and SAP HANA® are registered trademarks of SAP SE® in Germany and several other countries.
				</p>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" async defer
  src="https://apis.google.com/js/platform.js?publisherid=107596169826999779615">
</script>

<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">
twttr.conversion.trackPid('l4qzm');
</script>
		<noscript>
			<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l4qzm&p_id=Twitter" />
			<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l4qzm&p_id=Twitter" />
		</noscript>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/scrolltopcontrol.js"></script><!-- Scroll to top javascript -->
	 
	<?php wp_footer(); ?>
	</body>
</html>