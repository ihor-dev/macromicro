
	
<!-- Main body
================== -->
		<div class="wrapper">
			<div class="section-header"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-8 blog-summary-list">
		


		<!--blog post -->
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
						<div class="blog-summary">
							<a name="spoiler"></a><hr><h4><!--<h4 class="new-story">--><a href="<? the_permalink();?>"><? the_title();?></a></h4>
							<ul class="text-muted list-inline">
								<li><i class="fa fa-user"></i> by <? the_author();?></a></li>
								<li><i class="fa fa-calendar"></i> <?php the_time('jS F , Y'); ?></li>
								<li><i class="fa fa-map-marker"></i> <?=get_post_meta($post->ID, 'post_geo', 1); ?></li>
							</ul>
							<!-- content -->
							<? kama_excerpt("maxchar=800&save_format=<p><b><img>"); ?>

						</div>
				<?php endwhile; else: ?> 
					<p>
						<?php _e('Sorry, but page not found'); ?>
					</p> 
				<?php endif; ?>
		<!-- #blog post -->			
		


			
		  <!-- Pagination -->
						<? the_posts_pagination( array('end_size' => 2,'mid_size' => 2, 'prev_text'=>'&laquo;', 'next_text' => '&raquo;') );?>
					</div>
					<div class="col-sm-4">
		  <!-- Social Links -->
						<hr>		  
						<h3 class="hl">Social</h3>
						<div class="social-icons social-icons-default">
							<ul>
								<!--<li class="rss"><a href="#"><i class="fa fa-rss"></i></a></li>-->
								<li class="twitter"><a href="http://www.twitter.com/macromicrollc"><i class="fa fa-twitter"></i></a></li>
								<!--<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>-->
								<!--<li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>-->
								<!--<li class="github"><a href="#"><i class="fa fa-github"></i></a></li>-->
								<li class="linkedin"><a href="http://www.linkedin.com/company/macromicro"><i class="fa fa-linkedin"></i></a></li>
								<!--<li class="vk"><a href="#"><i class="fa fa-vk"></i></a></li>-->
								<!--<li class="plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
								<!--<li class="dropbox"><a href="#"><i class="fa fa-dropbox"></i></a></li>-->
								<!--<li class="vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>-->
							</ul>
							<div class="clearfix"></div>
							<div class="social-icons social-icons-default">
								<HR>
								<p>Press inquiries should be directed to <a href="mailto:info@macromicro.com" class="">info@macromicro.com</a> or +1 617 843 5030.
								</p><hr>
								<div class="clearfix"></div>
							</div>
						</div>
		  
		  <!-- Recently Added -->
						<? $bookmarks = get_bookmarks(array(
							'orderby'  => 'id',
							'order'	=> 'DESC',
							'category_name' => 'blog',
							'limit' => 6
							));
				        ?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Press Releases</h3>
							</div>
							<div class="panel-body">
								<div class="recent-blogs">
									<ul>
										<?foreach($bookmarks as $bm){?>
											<li><?=$bm->link_description;?><BR><a href="<?=$bm->likn_url;?>"><?=$bm->link_name;?></a>
										<?}?>										
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
